package com.arghanoa.cube;

import android.content.Context;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import static android.opengl.GLES20.*;


/**
 * Created by andrea on 03/05/16.
 */
public class Cube {
    private int program;


    private static final int BYTES_PER_FLOAT = 4;

    private FloatBuffer mVertexBuffer;
    private FloatBuffer mColorBuffer;
    private ByteBuffer  mIndexBuffer;
    private  FloatBuffer mTexturesBuffer;

    private float vertices[] = {

            -1f,-1f,1f,
            1f,-1f,1f,
            1f,1f,1f,
            -1f,1f,1f,

            -1f,-1f,-1f,
            1f,-1f,-1f,
            1f,1f,-1f,
            -1f,1f,-1f

    };

    //gli assi delle texture hanno la y che punta in basso
    /*
     ---------> x
    |
    |
    |
    V
    y
     */
    private float textures_UpDown[]={
            0.0f,1.0f,  //0
            0.0f,0.0f,  //1
            0.0f,1.0f,  //2
            0.0f,0.0f,  //3
            0.0f,0.0f,  //4
            1.0f,0.0f,  //5
            1.0f,1.0f,  //6
            0.0f,1.0f   //7
    };

    private byte indices_UpDown[] = {
            0,5,1,0,4,5,        //sotto
            3,2,6,3,6,7         //sopra
    };

    private float textures_LeftRight[]={
            0.0f,1.0f,  //0
            0.0f,1.0f,  //1
            0.0f,0.0f,  //2
            0.0f,0.0f,  //3
            1.0f,1.0f,  //4
            1.0f,1.0f,  //5
            1.0f,0.0f,  //6
            1.0f,0.0f   //7
    };

    private byte indices_LeftRight[] = {
            1,5,6,1,6,2,        //latodx
            0,7,4,0,3,7,         //latosx
    };

    private float textures_FrontBack[]={
            0.0f,1.0f,  //0
            1.0f,1.0f,  //1
            1.0f,0.0f,  //2
            0.0f,0.0f,  //3
            0.0f,1.0f,  //4
            1.0f,1.0f,  //5
            1.0f,0.0f,  //6
            0.0f,0.0f   //7
    };

    private byte indices_FrontBack[]={
            0,1,2,0,2,3,       //faccia dietro
            4,6,5,4,7,6,         //faccia davanti
    };


    private static final String A_POSITION = "a_Position";
    private int aPositionLocation;
    private static final String U_MATRIX = "u_Matrix";
    private int uMatrixLocation;
    private static final String A_TEXTURECOORDINATES = "a_TextureCoordinates";
    private int aTextureCoordinates;
    private static final String U_TEXTUREUNIT = "u_TextureUnit";
    private int uTextureUnit;

    private static final int POSITION_COMPONENT_COUNT = 3;
    private static final int TEXTURE_COMPONENT_COUNT = 2;


    /** This is a handle to our texture data. */
    private int mTextureDataHandle_UpDown;
    private int mTextureDataHandle_LeftRight;


    public Cube(Context context){

        mVertexBuffer = ByteBuffer.allocateDirect(vertices.length * BYTES_PER_FLOAT).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mVertexBuffer.put(vertices);
        mVertexBuffer.position(0);

        mTexturesBuffer = ByteBuffer.allocateDirect((textures_UpDown.length+textures_FrontBack.length+textures_LeftRight.length) * BYTES_PER_FLOAT).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTexturesBuffer.position(0);

        mIndexBuffer=ByteBuffer.allocateDirect((indices_UpDown.length+indices_FrontBack.length+indices_LeftRight.length)).order(ByteOrder.nativeOrder());
        mIndexBuffer.position(0);

        String vertexShaderSource = TextResourceReader.readTextFileFromResource(context, R.raw.texture_vertex_shader);
        String fragmentShaderSource = TextResourceReader.readTextFileFromResource(context, R.raw.texture_fragment_shader);


        int vertexShader = ShaderHelper.compileVertexShader(vertexShaderSource);
        int fragmentShader = ShaderHelper.compileFragmentShader(fragmentShaderSource);
        program = ShaderHelper.linkProgram(vertexShader, fragmentShader);

        mTextureDataHandle_UpDown = TextureHelper.loadTexture(context, R.drawable.up_down);
        mTextureDataHandle_LeftRight = TextureHelper.loadTexture(context, R.drawable.side);
    }

    public void draw(float[] uMatrix){

        glUseProgram(program);

        aPositionLocation = glGetAttribLocation(program, A_POSITION);
        uMatrixLocation = glGetUniformLocation(program, U_MATRIX);

        glVertexAttribPointer(aTextureCoordinates, TEXTURE_COMPONENT_COUNT, GL_FLOAT, false, 0, mTexturesBuffer);
        glEnableVertexAttribArray(aTextureCoordinates);

        aTextureCoordinates = glGetAttribLocation(program, A_TEXTURECOORDINATES);
        uTextureUnit = glGetUniformLocation(program, U_TEXTUREUNIT);

        glVertexAttribPointer(aPositionLocation, POSITION_COMPONENT_COUNT, GL_FLOAT, false, 0, mVertexBuffer);
        glEnableVertexAttribArray(aPositionLocation);

        glUniformMatrix4fv(uMatrixLocation, 1, false, uMatrix, 0);

        // Set the active texture unit to texture unit 0.
        glActiveTexture(GL_TEXTURE0);
        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
        glUniform1i(uTextureUnit, 0);

        //##UPDOWN

        mIndexBuffer.put(indices_UpDown);
        mIndexBuffer.position(0);
        mTexturesBuffer.put(textures_UpDown);
        mTexturesBuffer.position(0);

        // Bind the texture to this unit.
        glBindTexture(GL_TEXTURE_2D, mTextureDataHandle_UpDown);



        glDrawElements(GL_TRIANGLES, indices_UpDown.length, GL_UNSIGNED_BYTE, mIndexBuffer);

        mIndexBuffer.clear();
        mTexturesBuffer.clear();

        //##FRONTBACK

        mIndexBuffer.put(indices_FrontBack);
        mIndexBuffer.position(0);
        mTexturesBuffer.put(textures_FrontBack);
        mTexturesBuffer.position(0);

        // Bind the texture to this unit.
        glBindTexture(GL_TEXTURE_2D, mTextureDataHandle_LeftRight);

        glDrawElements(GL_TRIANGLES, indices_FrontBack.length, GL_UNSIGNED_BYTE, mIndexBuffer);

        mIndexBuffer.clear();
        mTexturesBuffer.clear();

        //##LEFTRIGHT

        mIndexBuffer.put(indices_LeftRight);
        mIndexBuffer.position(0);
        mTexturesBuffer.put(textures_LeftRight);
        mTexturesBuffer.position(0);

        // Bind the texture to this unit.
        glBindTexture(GL_TEXTURE_2D, mTextureDataHandle_LeftRight);

        glDrawElements(GL_TRIANGLES, indices_LeftRight.length, GL_UNSIGNED_BYTE, mIndexBuffer);

        mIndexBuffer.clear();
        mTexturesBuffer.clear();
    }
}
