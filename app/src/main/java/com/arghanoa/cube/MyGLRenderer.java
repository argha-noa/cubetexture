package com.arghanoa.cube;

import static android.opengl.GLES20.*;
import static android.opengl.GLUtils.*;
import static android.opengl.Matrix.*;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.Matrix;
import android.os.SystemClock;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;



/**
 * Created by andrea on 27/04/16.
 */
public class MyGLRenderer implements Renderer {


    private final Context context;

    private Cube mCube;

    private final float[] mModelMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];
    private final float[] mRotationMatrix = new float[16];
    protected float mDeltaX;
    protected  float mDeltaY;

    /** Store the accumulated rotation. */
    private final float[] mAccumulatedRotation = new float[16];

    /** Store the current rotation. */
    private final float[] mCurrentRotation = new float[16];




    public MyGLRenderer(Context context) {
        super();
        this.context=context;
    }

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //(R,G,B,Trasparency)
        mCube = new Cube(context);
        glEnable(GL_CULL_FACE);
        Matrix.setIdentityM(mAccumulatedRotation, 0);
    }

    @Override
    //public void onSurfaceChanged(GL10 glUnused, int i, int il)
    public void onSurfaceChanged(GL10 gl10, int width, int height)
    {
        glViewport(0,0,width,height);
        float ratio = (float) width / (float) height;

        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 0.7f, 5.0f);

    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        float[] tmp = new float[16];
        //pulisce la superficie di rendering
        glClear(GL_COLOR_BUFFER_BIT);
        // Set the camera position (View matrix)
        Matrix.setLookAtM(mViewMatrix, 0, 0, 0, 3f, 0f, 0f, 0f, 0f, 0.1f, 0.0f);

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mModelMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

        // Create a rotation transformation for the triangle
        //long time = SystemClock.uptimeMillis() % 40000L;
        //float angle = 0.0090f * ((int) time);

        // Set a matrix that contains the current rotation.
        Matrix.setIdentityM(mCurrentRotation, 0);
        Matrix.rotateM(mCurrentRotation, 0, mDeltaX, 0.0f, 1.0f, 0.0f);
        Matrix.rotateM(mCurrentRotation, 0, mDeltaY, 1.0f, 0.0f, 0.0f);

        //mDeltaX = 0.0f;
        //mDeltaY = 0.0f;

        mDeltaX =  mDeltaX/1.1f;
        mDeltaY = mDeltaY/1.1f;

        // Multiply the current rotation by the accumulated rotation, and then set the accumulated
        // rotation to the result.
        Matrix.multiplyMM(mRotationMatrix, 0, mCurrentRotation, 0, mAccumulatedRotation, 0);
        System.arraycopy(mRotationMatrix, 0, mAccumulatedRotation, 0, 16);

        // Combine the rotation matrix with the projection and camera view
        // Note that the mMVPMatrix factor *must be first* in order
        // for the matrix multiplication product to be correct.
        // Rotate the cube taking the overall rotation into account.
        Matrix.multiplyMM(tmp, 0, mModelMatrix, 0, mAccumulatedRotation, 0);

        mCube.draw(tmp);

    }

    public float[] mAngle=new float[2];

    public float getAngleX() {
        return mAngle[0];
    }

    public float getAngleY() {
        return mAngle[1];
    }

    public void setAngle(float angleX, float angleY) {
        mAngle[0] = angleX;
        mAngle[1] = angleY;
    }

}
